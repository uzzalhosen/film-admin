import { getToken } from '@/utils/auth'

const state = {
  token: getToken(),
  position: {}
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_POSITION: (state, position) => {
    state.position = position
  }
}

const actions = {
  setPosition({ commit }, position) {
    const { data } = position
    commit('SET_POSITION', position)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
