import request from '@/utils/apiRequest'

export function fetchDepartments() {
  return request({
    url: '/departments',
    method: 'get'
  })
}

export function fetchPersons() {
  return request({
    url: '/persons',
    method: 'get'
  })
}
export function filterPeopleByPosition(position) {
  return request({
    url: '/filterPeopleByPosition/' + position,
    method: 'get'
  })
}
export function getInfo(token) {
  return request({
    url: '/info/' + token,
    method: 'get'
  })
}
export function fetchPv(pv) {
  return request({
    url: '/article/pv',
    method: 'get',
    params: { pv }
  })
}
export function deletePosition(id) {
  return request({
    url: '/delete-position/' + id,
    method: 'get'
  })
}
export function deletePerson(id) {
  return request({
    url: '/delete-person/' + id,
    method: 'get'
  })
}
export function deleteDepartment(id) {
  return request({
    url: '/delete-department/' + id,
    method: 'get'
  })
}

export function editDepartment(data, id) {
  return request({
    url: '/departments/' + id,
    method: 'put',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function editPosition(data, id) {
  return request({
    url: '/update-position/' + id,
    method: 'put',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
export function editPerson(data, id) {
  return request({
    url: '/update-person/' + id,
    method: 'put',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
export function assignPosition(data) {
  return request({
    url: '/assign-position',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
export function importCsv(data) {
  return request({
    url: '/person/import/csv',
    method: 'post',
    data,
    headers: {
      // 'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
export function addPerson(data) {
  return request({
    url: '/add-person',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function sendEmail(data) {
  return request({
    url: '/send-mail',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function saveForm(data) {
  return request({
    url: '/save-form',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function removePosition(data) {
  return request({
    url: '/remove-position',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
export function addPosition(data) {
  return request({
    url: '/add-position',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
export function addCrew(data) {
  return request({
    url: '/add-crew',
    method: 'post',
    data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function updateArticle(data) {
  return request({
    url: '/article/update',
    method: 'post',
    data
  })
}
